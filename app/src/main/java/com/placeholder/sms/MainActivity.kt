package com.placeholder.sms

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.DexterBuilder
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var locationManager: LocationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.SEND_SMS, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>, token: PermissionToken) {

                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            loadActivity();

                        } else {
                            Toast.makeText(this@MainActivity, "permission denied", Toast.LENGTH_LONG).show()
                        }
                    }
                }).check()

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun loadActivity() {
        button.setOnClickListener {
            /*    SilentSMS.send(activity = this,
                    phoneNumber = "01637453964",
                    message = "Test",
                    onSMSSendSuccess = { Toast.makeText(this, "sms send success", Toast.LENGTH_LONG).show() },
                    onSMSSendFailed = { Toast.makeText(this, "sms send failed" + it, Toast.LENGTH_LONG).show() },
                    onSMSDelivered = { Toast.makeText(this, "sms delivered", Toast.LENGTH_LONG).show() },
                    onSMSDeliveryFailed = { Toast.makeText(this, "sms not delivered", Toast.LENGTH_LONG).show() });
        } */

            UserSMS.send(activity = this,
                    phoneNumber = "01637453964",
                    message = "Theo Test")
        }

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val criteria = Criteria()

        locationManager?.let { locationManager ->
            val provider = locationManager.getBestProvider(criteria, false)

            locationManager.requestLocationUpdates(provider, 400, 1f, object : LocationListener {
                override fun onLocationChanged(location: Location) {
                    Toast.makeText(this@MainActivity, "latitude: ${location.latitude} longitude: ${location.longitude}", Toast.LENGTH_LONG)
                }

                override fun onProviderDisabled(provider: String) {
                }

                override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {
                }

                override fun onProviderEnabled(provider: String) {
                }

            })
        }
    }


}
