package com.placeholder.sms

import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.telephony.SmsManager

object SilentSMS {
    enum class SendFailure {
        GENERIC_FAILURE,
        NO_SERVICE,
        NULL_PDU,
        RADIO_OFF
    }
    
    fun send(activity: Activity,
             phoneNumber: String,
             message: String,
             onSMSSendSuccess: () -> Unit,
             onSMSSendFailed: (SendFailure) -> Unit,
             onSMSDelivered: () -> Unit,
             onSMSDeliveryFailed: () -> Unit) {
        val SENT = "SMS_SENT"
        val DELIVERED = "SMS_DELIVERED"

        val sentPI = PendingIntent.getBroadcast(activity, 0,
                Intent(SENT), 0)

        val deliveredPI = PendingIntent.getBroadcast(activity, 0,
                Intent(DELIVERED), 0)

        //---when the SMS has been sent---
        activity.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (resultCode) {
                    Activity.RESULT_OK -> onSMSSendSuccess.invoke()
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> onSMSSendFailed.invoke(SendFailure.GENERIC_FAILURE)
                    SmsManager.RESULT_ERROR_NO_SERVICE -> onSMSSendFailed.invoke(SendFailure.NO_SERVICE)
                    SmsManager.RESULT_ERROR_NULL_PDU -> onSMSSendFailed.invoke(SendFailure.NULL_PDU)
                    SmsManager.RESULT_ERROR_RADIO_OFF -> onSMSSendFailed.invoke(SendFailure.RADIO_OFF)
                }
            }
        }, IntentFilter(SENT))

        //---when the SMS has been delivered---
        activity.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (resultCode) {
                    Activity.RESULT_OK -> onSMSDelivered.invoke()
                    Activity.RESULT_CANCELED -> onSMSDeliveryFailed.invoke()
                }
            }
        }, IntentFilter(DELIVERED))

        val smsManager = SmsManager.getDefault()

        smsManager.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI)
    }
}