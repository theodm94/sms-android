package com.placeholder.sms

import android.app.Activity
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.provider.Telephony
import android.os.Build


object UserSMS {
    fun send(activity: Activity, phoneNumber: String, message: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity)

            val sendIntent = Intent(Intent.ACTION_SEND)

            sendIntent.type = "text/plain"

            sendIntent.putExtra("address", phoneNumber)
            sendIntent.putExtra("sms_body", message)

            if (defaultSmsPackageName != null) {
                sendIntent.`package` = defaultSmsPackageName
            }

            activity.startActivity(sendIntent)
        } else {
            val smsIntent = Intent(android.content.Intent.ACTION_VIEW)

            smsIntent.type = "vnd.android-dir/mms-sms"

            smsIntent.putExtra("address", phoneNumber)
            smsIntent.putExtra("sms_body", message)

            activity.startActivity(smsIntent)
        }
    }
}